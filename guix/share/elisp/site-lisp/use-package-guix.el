;;; use-package-guix.el --- install emacs packages using GNU/Guix  -*- lexical-binding: t; -*-

;;; Copyright (C) 2021  Aron Gile(aarongile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;; The package is
(require 'cl-lib)
(require 'use-package)
(require 'gix-fs)
(require 'gix-log)

;;////////////////////////////////////////////////////////////////////////////
;;;  Use-package extension
;;;////////////////////////////////////////////////////////////////////////////
(defun guix-use-package-init ()
  ;; initialize :guix keyword
  (unless (member :guix use-package-keywords)
    (setq use-package-keywords
          (let* (
                 (pos  (cl-position :unless use-package-keywords))
                 (head (cl-subseq use-package-keywords 0 (+ 1 pos)))
                 (tail (nthcdr (+ 1 pos) use-package-keywords))
                 )
            (append head (list :guix) tail)))))

;;;###autoload
(defun use-package-normalize/:guix ( name:symbol keyword args )
  "Consumes the :guix keyword without performing any operation."
  (use-package-only-one
    (symbol-name keyword)   ;;label
    args                    ;;arguments
    `(lambda (_label  arg)  ;;functions
       arg)))

;;;###autoload
(defun use-package-handler/:guix (name:symbol keyword spec rest state)
  "Install Guix package specified by PACKAGE-SPEC.
- NAME-SYMBOL: symbolic name of the elisp-package
- KEYWORD: this is always :guix
- REST:  property list of the rest of the keywords applied to the use-package definition.
- STATE: custom data"

  (use-package-process-keywords name:symbol rest state))

(guix-use-package-init)
(provide 'use-package-guix)
;;; use-package-guix.el ends here
