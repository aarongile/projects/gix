* Guix

/GNU/Guix/ is a functional package and container environment management tool.
It can be used to create reproducible configurations as it supports
source level package management and transactional updates. Packages are
stored in a read-only file system =/gnu/store/= and changes to the store
are managed using a daemon capable of rolling-back modifications.

There are two approaches to installing GNU/Guix

 [[https://guix.gnu.org/manual/en/html_node/Installation.html][Source Install]] and   [[https://guix.gnu.org/manual/en/html_node/Binary-Installation.html][Binary Install]]

Source based install can be a bit involved and is also slowed to build.
This configuration will therefore cover only binary based installation.

** Install

The list of package managed by the configuration are the following:
#+begin_src scheme :tangle manifest
(specifications->manifest
'(
  ;; locale
   "glibc-locales"

   ;; X.509 Certs
   "nss-certs"
   "gnutls"
   "openssl"

   ;; font config
   "fontconfig"

   ;; Guile/scheme
   "guile"))
#+end_src
** Configuration

The list Guix packages managed by this configuration target additional configuration
steps [[https://guix.gnu.org/manual/en/html_node/Application-Setup.html][recommended]] when installing Guix on a foreign distro. The recommendation
includes installing and configuring:

*** Name Service Switch

*** TLS Configuration:.509 Certificates

Configure =nss-certs= package X.509 environment variables to allow
GNU/Guix packages to access SSL/TLS certificate files.

#+begin_src shell :tangle ./host.env
# TODO: this config is making SSL certs inaccessible.
#       Needs further investigation.

# GUIX_PROFILE="${HOME}/.cache/gix/guix/profiles/guix/guix"
# export SSL_CERT_DIR="${GUIX_PROFILE}/etc/ssl/certs"
# export SSL_CERT_FILE="${GUIX_PROFILE}/ssl/certs/ca-certificates.crt"
# export GIT_SSL_CAINFO="$SSL_CERT_FILE"
#+end_src

*** Shell Config: C Localization Library

GNU/Guix does not use the host systems locale data, instead it uses
its own install of the GNU C library locale library.
#+begin_src shell :tangle ./host.env
# default locale directory
export GUIX_LOCPATH=${GUIX_PROFILE}/lib/locale
#+end_src


*** X11 Fonts
 Here installing =fontconfig= is sufficient. Actual
 fonts will have to be installed by other gix
 components on a need basis.
