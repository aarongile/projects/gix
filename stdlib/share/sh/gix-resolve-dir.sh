# Copyright (C) 2021  Aron Gile(aarongile@gmail.com)
#
# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

# Author  : Aron Gile(aarongile@gmail.com)

######################################################################
# source
######################################################################
.  "${GIX_DIR}/commands/find/share/sh/gix-comp-find.sh"
#####################################################################
# functions
#####################################################################

gix_resolve_dir() {

    local _comp_name="$1"
    local _comp_slug="$2"
    local _comp_org_dir="$3"
    local _comp_dir="${HOME}/.cache/gix/cache/components/${_comp_name}"

    # path is contructed from org file?
    [ -e "${_comp_dir}/${_comp_slug}" ] && {
        echo "${_comp_dir}/${_comp_slug}"
        return 0
    }

    # path is builtin?
    local _comp_dir="${_comp_org_dir}"
    [ -d "${_comp_dir}" ] || _comp_dir="$(gix_comp_find ${_comp_name})"
    [ -e "${_comp_dir}/${_comp_slug}" ] && {
        echo "${_comp_dir}/${_comp_slug}"
        return 0
    }

    return 1
}
