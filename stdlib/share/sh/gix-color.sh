#########################################################
# Color
#
# Manage color of terminal output using tput
#########################################################

has_color() {

    # is tput available?
    command -v tput   2>&1 > /dev/null || return 1
    # is color off?
    [ "$GIX_COLOR" = "true" ] || return 1

    return 0
}

# check tput support or turn-off color
if has_color; then
    export GIX_COLOR_NONE="$(tput sgr0)"                       # default
    export GIX_COLOR_BOLD="$(tput bold)"                       # bold
    export GIX_COLOR_INFO="$(tput  setaf 3)"                   # yellow
    export GIX_COLOR_HINT="$(tput setaf 7)"                    # bright white
    export GIX_COLOR_WARN="${GIX_COLOR_BOLD}${GIX_COLOR_INFO}"   # dark yellow
    export GIX_COLOR_ERROR="$(tput setaf 1 sqr)"               # red
else
    export GIX_COLOR_NONE=""
    export GIX_COLOR_BOLD=""
    export GIX_COLOR_INFO=""
    export GIX_COLOR_HINT=""
    export GIX_COLOR_WARN=""
    export GIX_COLOR_ERROR=""
fi
