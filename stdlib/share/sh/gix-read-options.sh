# Copyright (C) 2021  Aron Gile(aarongile@gmail.com)
#
# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

# Author  : Aron Gile(aarongile@gmail.com)
# Comment :
#
# Read command-line options from file and writes it to stdout.
# It also expands expands any environmental variables prior .


#==============================================================================
# OPTIONS
#==============================================================================
set   -e
set   -o pipefail

function gix_read_options( )  {
    # Read shell command options from FILE.
    #
    # ARGS:
    # - FILE($1): command options file path
    #
    # Output:
    # FILE content is written to stdout after
    # striping comment lines and expanding any
    # environment variables
    #
    # RETURNS
    # zero on success otherwise nonzero

    local options_file=$1; shift
    [[ -f "${options_file}" ]] && {

        # strip comments, substitute variables,
        # and write output to stdout
        grep -o '^[^#]*' ${options_file} | envsubst | tee -a || return 1

    }

    return 0
}
