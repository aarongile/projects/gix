
. "${GIX_DIR}/stdlib/share/sh/gix-resolve-dir.sh"
. "${GIX_DIR}/commands/find/share/sh/gix-comp-find.sh"

gix_comp_run_options( ) {
    #
    # Returns container options to apply when running a component script
    #
    # ARGS:
    # - _COMP($1)    : name of the component
    # - _COMP_DIR($2): root directory of the component(optional)

    # component base directories
    local _comp="$1"
    local _comp_org_dir="$2"
    [ -d "${_comp_org_dir}" ] || {
	    _comp_org_dir=$(gix_comp_find "${_comp}")
        [ -d "${_comp_org_dir}" ] || {
		    gix_error "comp: unable to find component: ${_comp}"
	        return 1
        }
    }
    local _comp_dir="$(gix_resolve_dir ${_comp} )"


    # GIX_GUIX_PROFILES_DIR
    [  -d "${GIX_GUIX_PROFILES_DIR}" ]  || {
        gix_error "GIX_GUIX_PROFILES_DIR: ${GIX_GUIX_PROFILES_DIR:-[NONE]}"
        return 1
    }

    #########################################################
    ## file system for exposed Gix document
    #########################################################
    # usr/loca/bin
    local _usr_local_bin=$(gix_resolve_dir "${_comp}" 'bin')
    if [ -d "${_usr_local_bin}" ]; then
        echo "--expose=${_usr_local_bin}=/usr/local/bin"
    fi

    # usr/local/etc
    local _usr_local_etc="$(gix_resolve_dir ${_comp} 'etc')"
    if [ -d "${_usr_local_etc}" ]; then
        echo "--expose=${_usr_local_etc}=/usr/local/etc/${_comp}"
    fi

    # usr/local/share
    local _usr_local_share="$(gix_resolve_dir ${_comp} '/share')"
    if [ -d "${_usr_local_share}" ]; then
        echo "--expose=${_usr_local_share}=/usr/local/share/${_comp}"
    fi

    #############################################################
    # expose some gix directories as well
    #############################################################

    #expose: GIX_DIR -> /usr/local/share/gix/stdlib
    echo "--expose=${GIX_DIR}/stdlib=/usr/local/share/gix/stdlib"

    #expose: COMP_DIR -> /usr/local/share/gix/compdir
    echo "--expose=${GIX_COMPDIR}=/usr/local/share/gix/compdir"

    # .local
    echo "--share=${GIX_LOCAL_DIR}=/home/user/.local"

    #config
    echo "--share=${GIX_USER_CONFIG_DIR}=/home/user/.config"
    echo "--expose=${GIX_DIR}/guix/etc/container.env=/home/user/.bashrc"

    # cache
    echo "--share=${GIX_USER_CACHE_DIR}=/home/user/.cache"

    # guix
    echo "--expose=${GIX_GUIX_PROFILES_DIR}=/usr/local/etc/guix/profiles"

    # var
    echo "--share=${GIX_VAR_DIR}=/var"

    # project directiry
    echo "--share=${GIX_CWD}=/home/project/current"

    # tmp
    gix_mkdirp "${GIX_TMP_DIR}" || return 1
    echo "--share=${GIX_TMP_DIR}=/tmp"

    return 0
}
