# Copyright (C) 2021  Aron Gile(aarongile@gmail.com)
#
# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

# Author  : Aron Gile(aarongile@gmail.com)

gix_readenv ( ) {

    # Read environment variables from FILE($1) or stdin and expand it inline.
    #
    # The function also strips export keyword and expands environment
    # variables. This is useful when exporting environment variables
    # using env command
    #
    # Usage:
    # variables=$(cat variables.env | gix_readenv)
    # env ${variables} ...

    local _input=${1:-$(</dev/stdin)};
    echo "${_input}" | grep -v '^#' | sed -e 's/\<export\>//g' | xargs -d$'\n' echo || {
        return 1
    }

    return 0
}
