#! /usr/bin/env bash

# Copyright (C) 2021  Aron Gile(aarongile@gmail.com)
#
# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

# Author  : Aron Gile(aarongile@gmail.com)
# Comment :
# command-line prompt utils

PROMPT_COLOR_BOLD="$(tput  bold)"                # bold
PROMPT_COLOR_WARN="${COLOR_BOLD}${COLOR_INFO}"   # dark yellow

function gix_prompt_info ( ) {

    gix_hr
    echo  "${PROMPT_COLOR_BOLD}$1"
    tput init
    gix_hr
    return 0
}

function gix_prompt_yn ( ) {

    read -p "${PROMPT_COLOR_WARN}${1:-please select: [y/n]} " -n 1 -r
    echo ""
    [[ $REPLY =~ ^[Yy]$ ]] || {
        return 1
    }

    return 0
}
