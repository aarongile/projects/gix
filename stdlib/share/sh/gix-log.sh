######################################################################
# Log Utility
#
#  Redirect std output and error based on depending on the configured
#  log level using GIX_LOG_LEVEL and GIX_LOG_FILE
#
# - GIX_LOG_LEVEL:
#   Can be debug(default), warn, error, quite
#
# - GIX_LOG_FILE:
#   Set to filename to redirect std output and error output to
#   - If the variable is set to  directory path, the user has
#     to ensure it exists.
#   - Dependin on the log level, setting to this variable maybe ignored
##########################################################################

.  "${GIX_DIR}/stdlib/share/sh/gix-color.sh"

# set log level"
: ${GIX_LOG_LEVEL:=info}
case ${GIX_LOG_LEVEL} in
    debug|info)
        export stdout="${GIX_LOG_FILE:-/dev/stdout}"
        export stderr="${GIX_LOG_FILE:-/dev/stderr}"
        export stdwarn="${GIX_LOG_FILE:-/dev/stderr}"
        ;;
    warn)
        export stdout="/dev/null"
        export stdwarn="${GIX_LOG_FILE:-/dev/stderr}"
        export stderr="${GIX_LOG_FILE:-/dev/stderr}"
        ;;
    error)
        export stdout="/dev/null"
        export stdwarn="/dev/null"
        export stderr="${GIX_LOG_FILE:-/dev/stderr}"
        ;;
    quite)
        export stdout="/dev/null"
        export stdwarn="/dev/null"
        export stderr="/dev/null"
        ;;
    *)
        echo "[ERROR]: unknown log level: GIX_LOG_LEVEL=${GIX_LOG_LEVEL}"
        exit 1
	    ;;
esac


gix_info() {
    echo "${GIX_COLOR_INFO}==> ${1:- }${GIX_COLOR_NONE}" > ${stdout}
}
gix_subinfo() {
    echo "${GIX_COLOR_INFO}[INFO]: ${1:- }${GIX_COLOR_NONE}" > ${stdout}
}
gix_error() {
    echo "${GIX_COLOR_ERROR}[ERROR]: ${1:- }${GIX_COLOR_NONE}" > ${stderr}
}
gix_hint() {
    local hint="${1:-}"
    echo "${GIX_COLOR_HINT}${GIX_COLOR_BOLD}[HINT]: ${1:- }${GIX_COLOR_NONE}" > ${stdout}
}
gix_warn() {
    echo "${GIX_COLOR_WARN}[WARN]: ${1:- }${GIX_COLOR_NONE}" > ${stdwarn}
}
gix_attr() {
    local key="${1:- }"
    local value="${2:- }"
    echo "${GIX_COLOR_INFO}${1:- }${GIX_COLOR_NONE} : ${GIX_COLOR_BOLD}${2:- }${GIX_COLOR_NONE}" > ${stdout}
}
gix_fail() {
    berr "${1:-}"
    return 1
}

gix_hr() {
    #depreciated
    return 0
}
