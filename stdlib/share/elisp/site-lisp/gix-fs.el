;;; gix-fs.el --- file system utils -*- lexical-binding: t; coding: utf-8 -*-
;;;
;;; Copyright (C) 2021  Aron Gile(aarongile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;; Code:
;;;###autoload
(defun gix:fs/dir-ensure (dir)
  "Create =DIR= if it does not exist and return its path."
  (cl-assert (stringp dir) t "gix:fs/dir-ensure: DIR is a string type" )

  (let ((full-name (expand-file-name (substitute-in-file-name dir))))
    (unless (file-directory-p full-name)
      (shell-command-to-string (format "mkdir -p %s" full-name)))
    full-name))

;;;###autoload
(defun gix:fs/load-dir-ensure (dir)
  "Create =DIR= if it does not exist and add its path to `load-path'"

  (cl-assert
   dir
   'show-args
   "gix:fs/load-dir-ensure: parameter DIR must be string type" )
  (cl-assert
   (stringp dir)
   'show-args "gix:fs/load-dir-ensure: parameter DIR must be string type" )

  (let ((default-directory  (gix:fs/dir-ensure dir)))
    (add-to-list 'load-path default-directory)
    (setq load-path (delete-dups load-path))
    (normal-top-level-add-subdirs-to-load-path)
    default-directory))

;;;###autoload
(defun gix:fs/file-ensure ( file )
  "Create an empty file if FILE does not exist."

  (cl-assert (not (null    file)) 'show-args "gix:fs/load-dir-ensure: parameter DIR can't be nil" )
  (cl-assert (stringp file)       'show-args "gix:fs/file-ensure: parameter FILE must be of type string")

  (let ((full-name (expand-file-name (substitute-in-file-name dir))))
    (unless (file-exists-p full-name)
      (gix:fs/dir-ensure (file-name-directory full-name))
      (f-touch full-name))
    full-name))


;;;###autoload
(defun gix:fs/find-custom-el ( )
  "Open custom.el file."
  (interactive)
  (find-file  "/home/user/.config/preferences/emacs/custom.el"))

(provide  'gix-fs)
;;; gix-fs.el ends here
