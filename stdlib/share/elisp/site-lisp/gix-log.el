;;; gix-log.el --- log utilities -*- lexical-binding: t; coding: utf-8 *-
;;;
;;; Copyright (C) 2021  Aron Gile(aarongile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.

;; Commentary:
;; Code:

;; TODO: implement better Log process
;;       where error log includes source file
;;       and function name.


;;;###autoload
(defmacro gix:info ( msg &rest args)
  "Writes log message MSG into the *Messages* buffer.
When MSG is a format specifier ARGS can be used to pass
formatting arguments. See `format' for details"
  `(with-current-buffer (get-buffer-create "*Messages*")
     (let ((inhibit-read-only t))
       (message (concat
                 (cond
                  (load-file-name    (file-name-nondirectory load-file-name))
                  (buffer-file-name  (file-name-nondirectory buffer-file-name))
                  (t "*"))
                 ": "
                 ,msg)
                ,@args))))

;;;###autoload
(defmacro gix:sub-info ( msg &rest args)
  "MSG is prefixed with the string--'|_'.
A list of arguments ARGS can be specified if MSG is a format specifier."
  ;; (let ((prefixed-msg (format "|_ %s" msg)))
  `(with-current-buffer (get-buffer-create "*Messages*")
     (let ((inhibit-read-only t))
       (message (concat "|_ " ,msg) ,@args))))

(provide 'gix-log)
;;; gix-log.el ends here
