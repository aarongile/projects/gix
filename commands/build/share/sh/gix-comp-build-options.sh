# Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)

# This script is part of Gix.

# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.


#===============================================================================
# Build Command Helper Lib
#===============================================================================
. "${GIX_DIR}/stdlib/share/sh/gix-log.sh"

gix_comp_build_options() {
    # Construct build option for component
    # ARGS:
    # - COMP($1): component name
    # - COMP_DIR($2): component root directory
    # - MANIFEST($3): component manifest file
    # RETURNS:
    # zero on success otherwise nonzero

    local _comp="$1"
    local _comp_dir="$2"
    #local _manifest="--manifest=$3"

    ## COMP MANIFEST
    local _manifest="$3"
    if [ -f "${_manifest}" ]; then
	    local _manifest="--manifest=${_manifest}"
    else
	  unset _manifest
    fi


    # SHARED GUILE SITE
    # this is the directory where shared guile scheme modules are stored.
    local _shared_site="${GIX_DIR}/guix/share/guile/site-lisp"
    [ -d "${_shared_site}" ] || {
        gix_error "failed to find share Guix site-lisp: ${_shared_site}"
        return 1
    }
    _shared_site=--load-path=${_shared_site}

    ## COMPONENT DEFAULT SITE
    local _default_site="${_comp_dir}/share/guile/site-lisp"
    if [ -d "${_default_site}" ]; then
        _default_site=--load-path=${_default_site}
    else
        unset _default_site
    fi

    ## COMPONENT AUTO SITE
    local _auto_site="${HOME}/.cache/gix/cache/components/${_comp}/share/guile/site-lisp"
    if [ -d "${_auto_site}" ]; then
        _auto_site=--load-path=${_auto_site}
    else
        unset _auto_site
    fi

    ## COMP PROFILE
    local _profile="${HOME}/.cache/gix/guix/profiles/${_comp}"
    mkdir -p "${_profile}" || {
        gix_error "failed to create profile directory: ${_profile}"
        return 1
    }
    _profile="--profile=${_profile}/${_comp}"


    local _build_options=""
    IFS= read -r -d '\n' _build_options <<-EOF
${_shared_site}
${_default_site}
${_auto_site}
${_profile}
${_manifest}
EOF

    echo "${_build_options}" | sed -r '/^\s*$/d' | envsubst | tee -a
    return 0
}
