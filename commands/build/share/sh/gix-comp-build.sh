# Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)

# This script is part of Gix.

# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.


. "${GIX_DIR}/stdlib/share/sh/gix-log.sh"
. "${GIX_DIR}/stdlib/share/sh/gix-resolve-dir.sh"
. "${GIX_DIR}/commands/build/share/sh/gix-comp-build-options.sh"
. "${GIX_DIR}/commands/find/share/sh/gix-comp-find.sh"
. "${GIX_DIR}/commands/tangle/share/sh/gix-comp-tangle.sh"


gix_comp_build() {
    #
    # Install a Gix component given its name _COMP and
    # optionally its root directory _COMP_DIR
    #
    # ARGS:
    # - _COMP        : the name of the component
    # - _COMP_DIR    : the root directory of the component
    # RETURNS
    # zero on success otherwise non zero
    # NOTE:
    # ====
    # The build strategy is as follows
    # first tangle the component configuration file(when available)
    # Then, if the auto manifest or the default manifest has changed
    # build the component.

    # parse args
    local _comp="$1"
    local _comp_dir="$2"
    [ -d "${_comp_dir}" ] || {

        _comp_dir=$(gix_comp_find "${_comp}" ) || {
            gix_error "failed to find component: ${_comp_dir}"
            return 1
        }
    }

    # tangle
    gix_comp_tangle "${_comp}" "${_comp_dir}" || {
        gix_error "failed to tangle: ${_comp}"
        return 1
    }

    # check manifest
    {
        local _manifest=$(gix_resolve_dir "${_comp}" 'share/guile/site-lisp/manifest.scm' )
        gix_info "check manifest ${_manifest} ..."
        if [ -f "${_manifest}" ]; then
            local _manifest_lock="${GIX_USER_CACHE_DIR}/components/${_comp}/manifest.scm.lock"
            gix_info "check lock: ${_manifest_lock} ..."
            [ -f "${_manifest_lock}" ]                                       \
                && sha256sum -c "${_manifest_lock}"  1>& /dev/null           \
                && [ -d "${GIX_GUIX_PROFILES_DIR}/${_comp}" ]                \
                && {
                gix_info "check manifest $_manifest ...[OK]"
                return 0
            }
	        gix_info "check manifest $_manifest ... [BUILD NEEDED]"
        else
	        gix_info "check manifest $_manifest ... [OK]"
            return 0
        fi
    }


    # build
    {
        gix_info "build options"
        local _build_options=$(gix_comp_build_options ${_comp} ${_comp_dir} ${_manifest})
        gix_misc "${_build_options}"

        gix_info "build from manifest: ${_manifest} ..."
        guix package --verbosity=3 ${_build_options} || {
            gix_error "failed to install comp: ${_comp}"
            return 1
        }
        gix_info "lock build: ${_manifest_lock}"
        mkdir -p "$(dirname ${_manifest_lock})"
        sha256sum  "${_manifest}" > "${_manifest_lock}"
        gix_info "build from manifest: ${_manifest} ... [OK]"
    }

    return 0
}

gix_comp_build_workflows( ) {

    for workflow_org in $(find "${GIX_COMPDIR}/workflows" -type f -name '*.org')
    do
        local _workflow_dir=$(readlink -f "$(dirname "${workflow_org}")")
        local _workflow=$(basename    "${_workflow_dir}" )

        gix_info "found workflow <${_workflow}>"

        gix_comp_build "${_workflow}" "${_workflow_dir}"  || {
            gix_error "failed to build workflow: ${_workflow}"
        }

        GUIX_PROFILE="${HOME}/.cache/gix/guix/profiles/${_workflow}/${_workflow}"
        [ -f "${GUIX_PROFILE}/etc/profile" ] && {
            gix_info "loading workflow ..."
            . "${GUIX_PROFILE}/etc/profile" || {
                gix_error "failed to load GUIX profile for workflow: ${_workflow}"
            }
            gix_info "loading workflow ... [DONE]"
        }

        [ -d "${GUIX_PROFILE}/share/emacs/site-lisp" ] && {

            export EMACSLOADPATH="${GUIX_PROFILE}/share/emacs/site-lisp:${EMACSLOADPATH}"
        }

        gix_info "workflow ... [DONE]\n"
    done

    return 0
}
