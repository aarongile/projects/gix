# Author: Aaron Gile(aronggile@gmail.com)

# Copyright (C) 2021 <Aaron Gile(aronggile@gmail.com)>

# This script is part of Gix.

# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

#===============================================================================
# OPTIONS
#===============================================================================
set   -e
set   -o pipefail

#===============================================================================
# FUNCTIONS
#===============================================================================

gix_comp_find ( ) {

    local _c="$1"
    local _org_file="${PWD}/${_c}.org"
    [[ -f "${_org_file}" ]] && {
        echo "$PWD"
        return 0
    }
    gix_comp_find_internal "${_c}" "${GIX_DIR}"     && return 0
    gix_comp_find_internal "${_c}" "${GIX_COMPDIR}" && return 0

    return 1
}

gix_comp_find_internal( )  {

    # Retrun component root directory given its NAME & SEARCH_DIR
    # - NAME($1): name of component
    # - SEARCH_DIR($2): search directory
    # - RETURNS:
    # - 0 if component found, otherwise non-zero


    local comp="$1"
    local search_dir="$2"

    # sigle org-doc file?
    # ls -L $PWD/gix/commands/**/helpx/{bin,etc} 2&> /dev/null  | head -n 1
    local comp_org_file=$(find "${search_dir}" -name "${comp}.org" | head -n 1 || echo "")
    if [  -f "${comp_org_file}" ]; then
        # component bin dir
        local _comp_dir="$(dirname ${comp_org_file})"
        echo "${_comp_dir}"
        return 0
    fi

    # check if component is multifile component.
    # The expected file system layout should be of the form:
    # {comp}
    #  |
    #  |_ bin
    #  |_ etc
    #  |_ share
    local comps=$(find "${search_dir}" -type d -name "${comp}" || echo "")
    [ "${comps}" = "" ] && {
        return 1
    }
    echo "$comps" | tr ' ' '\n' | while read comp
    do
        if [ -d "${comp}/bin" ] || [ -d "${comp}/etc" ] || [ -d "${comp}/share" ]; then
            echo "${comp}"
            return 0
        fi
    done

    return 1
}
