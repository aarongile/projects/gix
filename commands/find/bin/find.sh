#!/usr/bin/env sh

# Copyright (C) 2021 <Aaron Gile(aronggile@gmail.com)>

# This script is part of Gix.

# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.


#===============================================================================
# IMPORTS
#===============================================================================
. "${GIX_DIR}/commands/find/share/sh/gix-comp-find.sh"
#===============================================================================
# MAIN
#===============================================================================

# options
set   -e
set   -o pipefail

# parse argument
gix_comp_name=$1
gix_comp_find "${gix_comp_name}" | tee -a ||  exit 1
