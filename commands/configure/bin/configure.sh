#! /usr/bin/env sh
# Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)

# This script is part of Gix.

# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

#========================================================================================
# IMPORTS
#========================================================================================
. "${GIX_DIR}/stdlib/share/sh/gix-log.sh"
. "${GIX_DIR}/stdlib/share/sh/gix-prompt.sh"
. "${GIX_DIR}/commands/build/share/sh/gix-comp-build.sh"

#========================================================================================
# Functions
#========================================================================================
gix_check_guix( ) {
    gix_info "check GNU/Guix installation ...\n"
    command -v guix &> /dev/null || {
        gix_error "Please, install GNU/Guix in your system before continuing"
        return 1
    }
}
gix_build_components_internal( ) {

    local _root_dir="$1"
    local _abort_error="$2"

    [ -d "${_root_dir}" ] || {
        gix_error "no such file(_root_dir): ${_root_dir}"
        return 1
    }

    gix_info "building components in: $1 \n"

    # for each configuration component that
    # implements a host shell environment configuration:
    for _comp in $(find "${_root_dir}" -type f -name '*-config.org' -o -name 'host.env')
    do
        local _comp_dir=$(dirname     "${_comp}" )
        local _comp_dir=$(readlink -f "${_comp_dir}/../")
        local _comp_name=$(basename   "${_comp_dir}")

        gix_info "build: ${_comp_name} ..."
        # build component
        gix_comp_build "${_comp_name}" "${_comp_dir}" "update-channels"
        gix_info "build: ${_comp_name} ... [OK]"
        echo

    done
    gix_info "\nbuilding components ... [OK]"

    return 0
}
gix_build_components( ) {

    gix_build_components_internal "${GIX_DIR}"     true || return 1
    gix_build_components_internal "${GIX_COMPDIR}" false

    return 0
}
gix_configure_components_internal( ) {

    local _root_dir="$1"
    local _abort_error="$2"

    [ -d "${_root_dir}" ] || {
        gix_error "no such file(_root_dir): ${_root_dir}"
        return 1
    }

    gix_info "configuring components in: ${_root_dir} \n"

    # for each configuration component that
    # implements a host shell environment configuration:
    for _comp_config_file in $(find "${_root_dir}" -path '**/bin/*-configure.sh')
    do
        [ -f "${_comp_config_file}" ] || {

            gix_misc "nothing to configure"
            gix_info "\nconfiguring components in: ${_root_dir} ... [DONE]"
            return 0

        }

        local _comp_dir=$(dirname     "${_comp}" )
        local _comp_dir=$(readlink -f "${_comp_dir}/../")
        local _comp_name=$(basename   "${_comp_dir}")
        local _comp_host_env_file="${_comp_dir}/etc/host.env"

        gix_info "configure: ${_comp_config_file} ..."

        # . host environment
        [ -f "{_comp_host_env_file}" ] && {

            . "${_comp_host_env_file}" || gix_warn "failed to source: ${_comp_host_env_file}"
        }

        # runt the config
        bash "${_comp_config_file}" && {
            gix_info "configure: ${_comp_config_file} ...  [OK]"
            continue
        }

        gix_info "configure: ${_comp_config_file} ...  [FAILED]"
        [ ${_abort_error} = true ] && {

            gix_error "a required component ${_comp_name} failed to build"
            gix_error "build will be terminated"
            return 1

        }

    done

                               return 0
}
gix_configure_components( ) {

    gix_configure_components_internal "${GIX_DIR}"     true || return 1
    gix_configure_components_internal "${GIX_COMPDIR}" false

    return 0
}
gix_bootstrap_host( ) {
    # TODO: resolve which system is in use here.
    return 0
}


#=========================================================================================
# Main
#=========================================================================================


gix_bootstrap_host       || exit 1
gix_check_guix           || exit 1
gix_build_components     || exit 1
gix_configure_components || exit 1

echo
gix_info "gix is configured and ready for use!!"
exit 0
