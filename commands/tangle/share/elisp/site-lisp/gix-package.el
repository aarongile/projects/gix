;;; gix-package.el --- Gix package interface -*- lexical-binding: t; coding: utf-8;  -*-
;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary
;; This interface defines the routine for building & installing
;; a GNU/Guix package using some package source.
;; The package source maybe the standard GNU/Guix package repository,
;; a git repo containing a Guix package, local file containing
;; package specification, and so on

(cl-defgeneric gix:package/build (using spec)
  "Returns a complete Guile package spec from minimal SPEC.

- SPEC: is :guix package specification plist.
- USING: is a specialization parameter it can take the following values:
        'channel: specification for channel based Guix package
        'git:     specification for git based Guix package
        'file:    specification for file based Guile package

Implimantation of this interface should resolve

1. :name     which is Guix package name for the elisp package
2. :args     zero or more arguments to pass for guix command

Implementation of this interface should resolve and add any property to SPEC and return the modifed SPEC.")


(provide 'gix-package)
