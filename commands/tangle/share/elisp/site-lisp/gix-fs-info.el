;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.




;;////////////////////////////////////////////////////////////////////////
;; misc
;;////////////////////////////////////////////////////////////////////////
;;;###autoload
(defconst gix:fs-guix-profiles-dir
  (let ((guix-profiles-dir (getenv "GIX_GUIX_PROFILES_DIR")))

    (unless (and guix-profiles-dir (file-directory-p guix-profiles-dir))
      (warn "unable to resolve GIX_GUIX_PROFILES_DIR"))

    (substitute-in-file-name guix-profiles-dir))
  "GUIX profiles directory")


;;///////////////////////////////////////////////////////////////////////
;; user level
;;///////////////////////////////////////////////////////////////////////
;;;###autoload
(defconst gix:fs-user-config-dir
  (let ((user-config-dir (getenv "GIX_USER_CONFIG_DIR")))
    (unless (and user-config-dir (file-directory-p user-config-dir))
      (warn "unable to resolve GIX_USER_CONFIG_DIR: %s" user-config-dir))
    user-config-dir)
  "User config directory basename.")


;;;###autoload
(defconst gix:fs-user-cache-dir
  (let ((user-cache-dir (getenv "GIX_USER_CACHE_DIR")))
    (unless (and user-cache-dir (file-directory-p user-cache-dir))
      (warn "unable to resolve GIX_USER_CACHE_DIR"))
    user-cache-dir)
  "User cache directory basename.")

;;///////////////////////////////////////////////////////////////////////
;; project  level
;;///////////////////////////////////////////////////////////////////////


;;;###autoload
(defconst gix:fs-project-config-dir
  (let ((project-config-dir (getenv "GIX_PROJ_CONFIG_DIR")))
    (unless (and project-config-dir (file-directory-p project-config-dir))
      (warn "unable to resolve GIX_PROJ_CONFIG_DIR"))
    project-config-dir)
  "Project config directory basename.")


;;;###autoload
(defconst gix:fs-project-cache-dir
  (let ((project-cache-dir (getenv "GIX_PROJ_CACHE_DIR")))
    (unless (and project-cache-dir (file-directory-p project-cache-dir))
      (warn "GIX_PROJ_CACHE_DIR"))
    project-cache-dir)
  "Project cache directory basename.")

;;;###autoload
(defconst gix:fs-gix-dir
  (let ((gix-dir (getenv "GIX_DIR")))

    (unless (and gix-dir (file-directory-p gix-dir))
      (warn "unable to resolve GIX_DIR"))

    gix-dir)
  "Gix root directory")


(provide  'gix-fs-info)
;;; gix-fs.el ends here
