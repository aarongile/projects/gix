;;; gix-package-git.el --- git based Guix package -*- lexical-binding: t; coding: utf-8;  -*-
;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.




(require 'gix-package)
(require 'gix-log)
(require 'gix-fs-info)
;;///////////////////////////////////////////////////////////////////////////////
;; Register GUIX package path
;;//////////////////////////////////////////////////////////////////////////////
(defconst gix:package--module-prefix  '(nonguix packages)
  "Namespace prefix for auto generated guix modules.")

(defconst gix:package--package-prefix "emacs"
  "Prefix Emacs package names with 'emacs' to enable proper auto-load file genearation by Guix")


(defun gix:package/guix-name ( spec )
  "Derive Guix package name from git SPEC"
  (let ((elisp-name (plist-get spec :elisp-name)))
    (format "%s-%s" gix:package--package-prefix elisp-name)))
(defun gix:package/guix-module-name (spec)
  "Retruns derived module name from package SPEC.
The module name has the form: {prefix}-{package-name}"
  (let* ((package-name (plist-get spec :name))
         (package-ns   ""))

    (unless (and package-name
                 (stringp package-name)
                 (not (string-empty-p package-name)))

      (setq package-name
            (gix:package/guix-name spec)))

    (cl-loop
     for name in gix:package--module-prefix
     do
     (setq package-ns (format "%s %s" package-ns name)))
    (format "%s %s" package-ns package-name)))

(defun gix:package--module/file-path (spec)
  "Returns the module file path derived from package SPEC."

  (let  ((comp-name       (plist-get spec :comp-name))
         (package-name    (plist-get spec :name))
         (manifest-info   (plist-get spec :manifest-info))
         (guile-site-dir   nil)
         (ns-path          "")
         (path           nil)
         )


    (unless (and
             package-name (stringp package-name)
             (not (string-empty-p package-name)))
      (setq package-name
            (gix:package/guix-name spec)))



    ;;ensure comp-name is defined
    (cl-assert (not (null comp-name))
               'show-args
               "comp-name is missing from spec: %s" spec)

    ;; construct namespace
    (cl-loop for name in gix:package--module-prefix
             do (setq ns-path  (format "%s/%s" ns-path name)))
    ;; construct module file path

    (setq guile-site-dir (plist-get manifest-info :guile-site-dir))
    (setq path (concat guile-site-dir
                       (if (string-empty-p ns-path) "" ns-path)
                       "/"
                       package-name
                       ".scm"))
    path))

(defun gix:package/assert (form &optional format-string &rest format-args)
  "Same as `cl-assert' except FORM is returned if the assertion passes.
  - FORM is the test expression
  - FORMAT-STRING is optional text message or message format.
  - FORMAT-ARGS   zero or more format arguments"
  (cl-assert
   form
   t
   (concat "gix:package/assert: FORM is nil" (or format-string "")) format-args)
  form)
(defmacro gix:package/replace-input ( input-form )
  ""
  `(let* ((input-tag         ,(symbol-name input-form))
          (replace-template   (upcase (format "{{%s}}" input-tag)))
          (input-list         (if (null ,input-form)
                                  nil
                                ,input-form))
          (replace-content    (if (null input-list) ""
                                (with-temp-buffer
                                  (insert  (format "\n (%s  `(\n" input-tag))
                                  (dolist  (input input-list)
                                    (insert (format " (\"%s\"  ,%s)\n"
                                                    (car input)
                                                    (cdr input))))
                                  (insert "  ))\n")
                                  (buffer-string)))))
     ;; find template position
     (goto-char (point-min))
     (unless (search-forward  replace-template nil 'NOERROR 1)
       (error "gix-package.el: failed to find place-holder [%s] in template file"
              replace-template))
     ;; replace match with replace content
     (replace-match  replace-content t)))
(defun gix:package/replace-form ( placeholder form &optional multi-replace )
  ""
  (goto-char (point-min))
  (unless (search-forward placeholder nil t)
    (error   "gix-package-using-git.el: gix:package/replace-form ...failed to perform substitution"))
  (gix:sub-info "subistituting: <%s> --> <%s>" placeholder form)
  ;;(replace-match (format "%s" form) t)
  (replace-match "")
  (insert (format "%s" form))
  (when multi-replace
    (goto-char (point-min))
    (while (search-forward placeholder nil t)
      (replace-match "")
      (insert (format "%s" form) )
      (gix:sub-info "subistituting: <%s> --> <%s>" placeholder form)
      (goto-char (point-min)))))




;;;///////////////////////////////////////////////////////////////////////
;;; main
;;;///////////////////////////////////////////////////////////////////////
(cl-defmethod gix:package/build ( (using (eql git)) spec )
  "Run configuration needed prior to installing git hosted package"

  ;; spec for git based installation does not contain package :name
  ;; The :name is instead auto generated from the elisp-name of the package


  (setq spec (plist-put spec :name (gix:package/guix-name spec)))
  (let* (

         (elisp-name             (plist-get  spec :elisp-name))
         (package-name           (plist-get  spec :name))
         (comp-name              (plist-get  spec :comp-name))

         (guile-site-dir         (expand-file-name
                                  (format  comp-name "%s/guile/site-lisp")
                                  gix:fs-user-cache-dir))

	     (module-template-file   (expand-file-name
                                  "commands/tangle/share/guile/templates/template-module.scm"
                                  gix:fs-gix-dir))

         (module-name            (gix:package/guix-module-name spec))
	     (module-file            (gix:package--module/file-path spec))

         )

    (gix:info "configuring package <%s> ...." elisp-name)
    (gix:sub-info "bulding Guix module script....")
    (gix:sub-info "component: %s"  comp-name)
    (gix:sub-info "module:    %s"  module-name)
    (gix:sub-info "load path: %s"  guile-site-dir)
    (gix:sub-info "file:      %s"  module-file)
    (gix:sub-info "template:  %s"  module-template-file)

    ;; ensure module directory is available
    (gix:fs/dir-ensure (file-name-directory module-file))

    ;; generate module file from template file:
    ;; {distro-config}/emacs/var/guix/modules/nongnu/packages/gix:{module-name}.scm
    ;; Note
    ;; ====
    ;; start with module template file and use search and replace place holder tags.
    (gix:sub-info "applying template substitutions ....")
    (with-temp-buffer

      ;; insert template content
      (insert-file-contents module-template-file)
      (goto-char (point-min))

      ;; module
      (gix:package/replace-form "{{MODULE}}" module-name  t)

      ;;use-modules
      (let* ((use-modules      (plist-get spec :use-modules))
             (use-modules-form (with-temp-buffer
                                 (dolist (use-module use-modules)
                                   (insert (format "#:use-module %s\n" use-module)))
                                 (goto-char (point-min))
                                 (while (search-forward "prefix" nil t)
                                   (replace-match "#:prefix" t))
                                 (goto-char (point-max))
                                 (buffer-string))))
        (gix:package/replace-form "{{USE-MODULES}}" use-modules-form nil))

      ;; package
      (gix:package/replace-form "{{PACKAGE}}" package-name t)

      ;; version
      (let ((version  (plist-get spec :version))
            (revision (or (plist-get spec :revision) "0")))
        ;; version information
        ;; Clearly, we need to make the commit identifier of the VCS
        ;; snapshot visible in the version string, but we also
        ;; need to make sure that the version string is monotonically
        ;; increasing so that guix package --upgrade can determine which version is newer.
        ;; Since commit identifiers, notably with Git, are not monotonically increasing,
        ;; we add a revision number that we increase each time we upgrade to a
        ;;newer snapshot. The resulting version string looks like this:
        ;; 2.0.11-3.cabba9e
        ;; ^    ^    ^
        ;; |    |    `-- upstream commit ID
        ;; |    |
        ;; |    `--- Guix package revision
        ;; |
        ;; latest upstream version

        (gix:package/replace-form "{{VERSION}}"  version)
        (gix:package/replace-form "{{REVISION}}" revision))

      ;; source
      (let* ((source    (gix:package/assert (plist-get spec   :source)     ":source   is required property"))
             (git       (gix:package/assert (plist-get source :git)        ":git      is required property"))
             (commit    (gix:package/assert (plist-get source :commit)     ":commit   is required property"))
             (revision  (gix:package/assert (plist-get source :revision)   ":revision is required property"))
             (sha256    (gix:package/assert (plist-get source :sha256)     ":sha256   is required property")))
        ;;TODO automatically download and determine
        ;; latest commit
        ;; sha256
        ;; revision
        ;; version
        ;; git
        (gix:package/replace-form "{{GIT}}" git nil)
        ;; commit
        (gix:package/replace-form "{{COMMIT}}" commit nil)
        ;; sha256
        (gix:package/replace-form "{{SHA256}}" sha256 nil))

      ;;inputs
      (let ((inputs   (plist-get spec :inputs))
            (propagated-inputs (plist-get spec :propagated-inputs))
            (native-inputs     (plist-get spec :native-inputs)))
        (gix:package/replace-input  inputs)
        (gix:package/replace-input  native-inputs)
        (gix:package/replace-input  propagated-inputs))

      ;; build-system
      (let ((build-system  (plist-get spec :build-system)))
        (when (null build-system)
          (setq build-system 'emacs-build-system))
        (gix:package/replace-form "{{BUILD-SYSTEM}}" build-system nil))

      ;; arguments
      (let ((copy-file-list            (plist-get spec :copy-files))
            (copy-dir-list             (plist-get spec :copy-dirs))
            (elisp-dir                 (plist-get spec :elisp-dir))
            (disable-byte-compilation  (plist-get spec :disable-byte-compilation))
            (template-arg
             "(arguments

               `(#:phases
                (modify-phases %standard-phases

                 (add-after 'unpack 'find-elisp-source-directory
                   ;; Elisp directory is not in root of the source.
                  (lambda _  (chdir {{elisp-dir}} ) #t))


                 {{disable-byte-compilation}}

                 (add-after 'install 'install-resource-files
                 (lambda* (#:key outputs #:allow-other-keys)
                        (let* ((out            (assoc-ref outputs \"out\"))
                               (site-lisp      (string-append (elpa-directory out) \"/\" ))
                               (copy-file-list {{copy-file-list}} )
                               (copy-dir-list  {{copy-dir-list}} ))
                          ;;ensure output directory
                          (mkdir-p site-lisp)

                          ;;copy each file in copy-file-list
                          (for-each

                           (lambda (src-file)
                           (define dest-file (string-append site-lisp src-file))
                           (copy-file src-file dest-file)
                           (format #t \"file copy:  ~a --> ~a~@\" src-file dest-file))

                           copy-file-list)

                         ;;copy each file in copy-dir-list
                        (for-each

                           (lambda (src-dir)
                           (define dest-dir (string-append site-lisp src-dir))
                           (mkdir-p dest-dir)
                           (copy-recursively src-dir dest-dir)
                         ;;(format #t \"copy directory: ~a --> ~a~@\" src-dir dest-dir)
                           )

                          copy-dir-list))
                        #t)))))"))

        (setq template-arg
              (with-temp-buffer

                (text-mode)
                (insert template-arg)

                ;;copy extra elisp directories
                (goto-char (point-min))
                (search-forward "{{elisp-dir}}")
                (if (null elisp-dir)
                    (replace-match "\".\"")
                  (replace-match (format "\"%s\"" elisp-dir)))

                ;;copy files
                (goto-char (point-min))
                (search-forward "{{copy-file-list}}")
                (if (null copy-file-list)
                    (replace-match "(list )")
                  (replace-match "'( ")
                  (cl-loop for file in copy-file-list
                           do (insert "\"" (format "%s" file) "\" "  ))
                  (insert " )"))

                ;;copy directories
                (goto-char (point-min))
                (search-forward "{{copy-dir-list}}")
                (if (null copy-dir-list)
                    (replace-match "(list )")
                  (replace-match "'( ")
                  (cl-loop for dir in copy-dir-list
                           do (insert "\"" (format "%s" dir) "\" "))
                  (insert " )"))

                ;; disable byte compilation
                (goto-char (point-min))
                (search-forward "{{disable-byte-compilation}}")
                (if (null disable-byte-compilation)
                    (replace-match "")
                  (replace-match "
                               (delete 'make-autoloads)
                               (delete 'enable-autoloads-compilation)"))

                (buffer-string)))
        (gix:package/replace-form "{{ARGUMENTS}}" template-arg nil))

      ;; generate the file
      (write-region nil nil module-file)
      ;;(gix:info "\nGenerated script file:\n %s\n" (buffer-string))
      )
    (gix:info "configuring package <%s> ....[OK]" elisp-name)

    ;;return results
    spec))


(provide 'gix-package-git)
