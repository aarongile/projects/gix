;;; gix-package-file.el --- local file based Guix package -*- lexical-binding: t; coding: utf-8;  -*-

;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary
;; Local file based package config & installation
;; Possible specifications:
;; :guix (:using file :name <package-name> )
;; :guix (:using file )

(defun gix:package/name-from-filespec ( spec )
  "Returns Guix package name from file SPEC"
  (let ((package-name  (plist-get spec :name)))
    (cond
     (;; null package name?
      (not package-name)
      (gix:info "file based Guix package specification for <%s> is missing"(plist-get spec :elisp-name))
      (setq package-name (format "emacs-%s" elisp-name))
      (gix:sub-info "guessed name: %s" package-name)
      package-name)
     (;; symbolic package name?
      (symbolp package-name)
      (format "%s" package-name))
     (;; string name?
	  (stringp package-name)
      package-name)
     (t ;; invalid name?
      (gix:info "invalid package name")
      (gix:sub-info "elisp package: %s" (plist-get spec :elisp-name))
      (gix:sub-info "invalid nagix:  %s" package-name)
      (error "invalid package name type")))))
(cl-defmethod gix:package/build( (using (eql file)) (spec t) )
  "Run configuration prior to installing Guix package specificed by SPEC from file.
- USING     'file
- SPEC       package guix specification"
  (let  (
         (comp-name          (plist-get spec :comp-name))
         (elisp-name            (plist-get spec :elisp-name))
         (package-name          (plist-get spec :name))
         )

    ;; ensure proper package name
    (unless package-name
      (setq package-name (gix:package/name-from-filespec spec))
      (setq spec         (plist-put spec :name package-name)))

    (gix:info "configuring file install process for ...." elisp-name)
    (gix:sub-info "package:     %s" package-name)

    ;; we only need to check if the module-file exists
    ;; and assume the user has placed the file in
    ;; a proper auto-profile directory
    ;; (unless (file-exists-p module-file)
    ;;   (gix:info "unable to install package: <%s>" elisp-name)
    ;;   (gix:sub-info "file not found: <%s>" module-file)
    ;;   (user-error "unable to install package <%s> from file %s" elisp-name module-file))

    ;; return result
    spec))




(provide 'gix-package-file)
