;;; gix-guix.el --- parses and extracts GNU/Guix package spec list from Emacs package file -*- lexical-binding: t; -*-

;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(require 'subr-x)
(require 'cl-lib)
(require 'thingatpt)

(require 'gix-fs)
(require 'gix-log)

(require 'gix-fs-info)
(require 'gix-package)
(require 'gix-package-channel)
(require 'gix-package-git)
(require 'gix-package-file)

;; /////////////////////////////////////////////////////////////////////////////
;; main
;; /////////////////////////////////////////////////////////////////////////////

(cl-defun gix:manifest/init ( comp-name comp-dir &optional (cache-dir gix:fs-user-cache-dir) )
  "Init a new manifest build information given its component COMP-NAME and COMP-DIR
Where
COMP-NAME: is the component's  name
COMP-DIR:  is the component's  root directory

Additionaly this function can accept a CACHE-DIR, for caching output files.
The value of cache-dir defaults to `gix:fs-user-cache-dir'"

  (list :package-specs            nil
        :cache-dir                cache-dir

        :comp-name                comp-name
        :comp-dir                 comp-dir
        :comp-cache-dir           (expand-file-name comp-name cache-dir)

        :default-elisp-site-dir   (expand-file-name "share/elisp/site-lisp" comp-dir)
        :default-guile-site-dir   (expand-file-name "share/guile/site-lisp" comp-dir)
        :default-manifest         (expand-file-name "share/guile/site-lisp/manifest.scm" comp-dir)

        :bin-dir                  (expand-file-name (format "components/%s/bin"   comp-name)  cache-dir)
        :etc-dir                  (expand-file-name (format "components/%s/etc"   comp-name)  cache-dir)
        :share-dir                (expand-file-name (format "components/%s/share" comp-name)  cache-dir)
        :elisp-site-dir           (expand-file-name (format "components/%s/share/elisp/site-lisp" comp-name)  cache-dir)
        :manifest                 (expand-file-name (format "components/%s/share/guile/site-lisp/manifest.scm" comp-name) cache-dir)
        :guile-site-dir           (expand-file-name (format "components/%s/share/guile/site-lisp" comp-name)  cache-dir)))

(cl-defun gix:manifest/scan ( manifest-info source-file )
  "Scan SOURCE-FILE for Guix package specifications.

The side-effect of this function is to update MANIFEST-INFO's
:package-specs list with package specs scaned from
the source-file.

- MANIFEST-INFO: is a plist with entries defined in `gix:manifest/init'
"
  (let ((package-name        nil)
        (package-spec        nil)
        (comp-name           (plist-get manifest-info :comp-name))
        (package-specs       (plist-get manifest-info :package-specs))
        (start-pos           nil)
        (end-pos             nil))

    (gix:info "scanning: %s" source-file)
    (with-temp-buffer

      (insert-file-contents source-file)
      (goto-char (point-min))

      ;; TODO: for each line containing a use-package expression
      ;; that is not a comment
      (while (search-forward "(use-package" nil t)

        ;; cursor pos: ... (use-package|
        (save-excursion
          ;; move cursor to: (use-package NAM|E
          (forward-sexp)
          (setq package-name (format "%s" (sexp-at-point))))


        ;; cursor pos: (use-package| ..
        ;; move it to: |(use-package
        (backward-up-list)

        (save-excursion
          ;; read the start and end pos of use-package sexp
          (setq start-pos (point))
          (thing-at-point--end-of-sexp)
          (setq end-pos   (point)))

        ;; narrow buffer to the current use-package
        ;; sexp only
        (narrow-to-region start-pos end-pos)
        (goto-char (point-min))
        ;;DEBUG
        ;;(message "REGION: \n%s\n" (buffer-string))

        ;; now search for a :guix keyword
        (when (search-forward ":guix" nil t)
          ;; read the :guix spec
          ;; (use-package ... :guix[HERE]

          ;;read guix specification
          (forward-sexp)
          (setq package-spec (sexp-at-point))

          ;; add component name
	      (setq package-spec
                (plist-put package-spec :comp-name comp-name))

          ;; add elisp name
	      (setq package-spec
                (plist-put package-spec :elisp-name (intern package-name)))

          ;; log
          (gix:info      "package: %s"     package-name)
	      (gix:sub-info  "guix-spec: %s\n" package-spec)

          ;; add back-ref to manifest

          (setq package-spec (plist-put package-spec :manifest-info manifest-info))

          ;; collect package spec
	      (cl-pushnew
           package-spec
           package-specs))

        ;; cursor cursor to the end of use-package
        (goto-char (point-max))

        (widen)
        ;;(forward-list)
        ))

    ;; return result
    (setq manifest-info
          (plist-put manifest-info :package-specs package-specs))

    (gix:info "scanning: %s ... [DONE]" source-file)
    manifest-info
    ))
(cl-defun gix:manifest/build( manifest-info )
  "Construct manifest from PACKAGE-SPECS."
  (let* ((package-specs                (plist-get manifest-info :package-specs))
         (comp-name                    (plist-get manifest-info :comp-name))
	     (comp-dir                     (plist-get manifest-info :comp-dir))
         (guile-site-dir               (plist-get manifest-info :guile-site-dir))
         (elisp-site-dir               (plist-get manifest-info :elisp-site-dir))
         (manifest-file                (plist-get manifest-info :manifest))
         (default-manifest-file        (plist-get manifest-info :default-manifest))
	     )

    ;; if there is no default manifest and
    ;; the package spec list is empty there
    ;; is nothing to be done
    (unless (or (file-exists-p default-manifest-file)
                (> (length package-specs)  0))
      (cl-return-from gix:manifest/build))

    ;;build manifest file
    (with-temp-buffer

      ;; use-modules specifications
      (cl-loop
       for spec in package-specs
       do
       (let ((use-modules  (plist-get spec :manifest-use-modules)))
         (unless (null use-modules )
           (insert (format "%s" use-modules) " \n"))))
      (goto-char (point-max))

      ;; begin default manifest
      (when (file-exists-p default-manifest-file)
        (insert "(use-modules (guix profiles))\n" )
        (insert "(concatenate-manifests (list\n"  )

        ;; begin default manifest
        (insert ";; BEGIN default manifest\n" )
        (goto-char (point-max))
        (insert-file-contents default-manifest-file)
        (goto-char (point-max))
        (insert "\n")
        (insert ";; END default manifest\n" ))
      ;; end default manifest

      ;; begin auto manifest
      (goto-char (point-max))
      (insert ";; BEGIN auto manifest\n")
      (insert "(specifications->manifest '(")
      (cl-loop
       for spec in package-specs
       do
       (let ((package-name           nil)
             (package-manifest-name  nil)
             (package-use-module     nil)
	         (using   (plist-get spec :using)))
         ;; build package spec
         (setq spec (gix:package/build using spec))
         ;; add package to auto manifest
	     (setq package-name          (plist-get spec :name ))
         (setq package-manifest-name (plist-get spec :manifest-name ))
         (when package-manifest-name (setq package-name package-manifest-name))
         (insert "\n" "\"" (format "%s" package-name) "\"")))
      ;;; end auto manifest

      (insert "))\n" )
      (insert ";; END auto manifest\n")

      ;; end of concatente-manifest
      (when (file-exists-p default-manifest-file)
        (insert "))"))

      ;; reduce multiple empty lines to single empty line
      (goto-char (point-min))
      (replace-regexp "^\n+" "\n")

      ;;save manifest file
      (gix:sub-info "mkdir: %s" guile-site-dir)
      (gix:fs/dir-ensure        guile-site-dir)
      (write-region nil nil manifest-file))))


;;; ////////////////////////////////////////////////////////////////////////////
;;; Test
;;; ////////////////////////////////////////////////////////////////////////////
(ert-deftest gix:manifest/init-test ()
  "Test `gix:manifest/init'"
  (should
   (equal "emacs" (plist-get  (gix:manifest/init "emacs") :comp-name))))


(provide 'gix-manifest)
;;; gix-guix.el ends here
