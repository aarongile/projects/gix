;;; gix-package-channel.el --- Channel Based Guix Package -*- lexical-binding: t; coding: utf-8;  -*-

;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.



(require 'gix-package)
(require 'gix-log)

;;;/////////////////////////////////////////////////////////////////////
;;; Channel based automatic package config & installation
;;;/////////////////////////////////////////////////////////////////////
(cl-defmethod gix:package/build ( (using (eql channel)) spec )
  (let ((elisp-name   (plist-get spec :elisp-name))
        (name         (plist-get spec :name)))

    ;; log config info
    (gix:info "configuring package: <%s> from <%s> ..." elisp-name name)
    (gix:sub-info "using: channel")
    ;; try guessing package name from elisp-name
    ;; if it is not already specified
    (unless name
      (setq name (format "emacs-%s" elisp-name))
      (setq spec (plist-put spec :name name))
      (gix:sub-info  "Guix package name is not specified for <%s> package." elisp-name)
      (gix:sub-info  "guessing package name as: <%s>" name))

    ;;return the configured spec
    spec))




(provide 'gix-package-channel)
