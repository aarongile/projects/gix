;;; gix-litconfig.el --- Literate Config Reader -*- lexical-binding: t; coding: utf-8;  -*-

;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.

(require 'cl-lib)
(require 'gix-log)
(require 'gix-fs)
(require 'gix-fs-info)
(require 'gix-manifest)
(require 's)
;;//////////////////////////////////////////////////////////////////////////
;; variables
;;//////////////////////////////////////////////////////////////////////////
(defconst gix:litconfig-shebang-patterns
  (list "^#!/usr/.*/perl\\(\\( \\)\\|\\( .+ \\)\\)-w *.*"
	    "^#!/usr/.*/bash"
        "^#!bash"
        "^#!/bin/bash"
	    "^#!/bin/sh"
	    "^#!/usr/.*/sh"
        "^#!zsh"
	    "^#!/bin/zsh")
  "A list of file headers that identify a source code as a script.")

;;////////////////////////////////////////////////////////////////////////////
;; utility functions
;;////////////////////////////////////////////////////////////////////////////
(defun gix:litconfig/cache-delete-files (cache-dir)
  "Delete all files in a chache directory CACHE-DIR"

  (when (file-directory-p  cache-dir )
    (gix:info  "delete cached files in: <%s> ..." cache-dir )

    (cl-loop
     for file in (directory-files cache-dir t)
     if (not (or (string-suffix-p "/." file) (string-suffix-p "/.." file)))
     do
     (progn (gix:sub-info "delete: %s ..." file)
            (unless (file-directory-p file)
              (gix:info "delete: %s" file)
              (delete-file file t))))

    (gix:info  "delete cached files in: <%s> ... [DONE]" cache-dir )))
(cl-defun gix:litconfig/save-buffer-as (file-name)
  "Save the current buffer as FILE-NAME.
If the buffer contains a shebang header mark it as an executable.

References
==========
[1]: https://www.emacswiki.org/emacs/MakingScriptsExecutableOnSave"


  (cl-assert (and file-name (stringp file-name) (> (length file-name) 0))
             'SHOW-ARGS
             "file-name must be non empty string")
  ;; write buffer to file
  (let ((file-dir (file-name-directory file-name)))
    (gix:fs/dir-ensure file-dir)
    (write-region (point-min)
                  (point-max)
                  file-name))
  (cl-assert (file-exists-p file-name))
  ;;if it is not already executable and contains a shebang header
  ;;change file to an executable script
  (let ((io-buffer (get-buffer-create "*Shell Command Output*")))
    (unless (= 0 (shell-command (concat "test -x " file-name)
                                io-buffer
                                io-buffer))
      ;; This puts message in *Message* twice, but minibuffer
      ;; output looks better.
      (gix:info (format "write: %s" file-name) )
      (save-excursion
        (goto-char (point-min))
	    ;; Always checks every pattern even after
	    ;; match.  Inefficient but easy.
	    (cl-loop
         for shebang-pattern in gix:litconfig-shebang-patterns
	     if (looking-at shebang-pattern)
         do
	     (if (= 0 (shell-command
                   (concat "chmod u+x " file-name)
                   io-buffer
                   io-buffer ))

             (progn (gix:sub-info "made executable: %s" file-name)
                    (cl-return-from gix:litconfig/save-buffer-as nil))

           (gix:sub-info "[FAILED] make executable: %s" file-name)
           (cl-return-from  gix:litconfig/save-buffer-as))))

      (kill-buffer io-buffer)


      )))
(cl-defun gix:litconfig/ignore-current-section-p ( )
  "Return t whenever the cursor is positioned inside a TODO|IGNORE node child source-block

   that is contained in a section tagged TODO or IGNORE. Otherwise it will
   return nil. This utility is used for ignoring literate
   configuration section that is tagged with TODO or IGNORE."

  (save-excursion
    (if (re-search-backward "^\\*+\\s-+\\(.*?\\)?\\s-*$" nil t)
        (or
         (string-prefix-p "TODO"   (match-string 1))
         (string-prefix-p "IGNORE" (match-string 1)))

      nil)
    ))
(cl-defun gix:litconfig/comp-dir-from-org (org-file)
  "Returns the configuration component directory given the ORG-FILE"
  (let ((org-dir (file-name-directory org-file)))
    ;; see
    ;; :https://emacs.stackexchange.com/questions/9554/function-that-returns-parent-directory-absolute-path
    (unless (equal "/" org-dir)
      (file-name-directory (directory-file-name org-dir)))))
(cl-defun gix:litconfig/comp-name-from-org (org-file)
  "Return configuration name for ORG-FILE"
  (let ((lib-basename (file-name-nondirectory org-file))
        (lib-name))
    (setq lib-name
          ;; make lib name from org file
          (if (string-suffix-p "-config.org" lib-basename)
              (substring lib-basename 0 -11)
            (substring lib-basename 0 -4)))))
(cl-defun gix:litconfig/unescape-code-in-region (beg end)
  "Un-escape lines between #+BEG and #+END directives.

Un-escaping happens by removing the first comma on lines starting
with \",*\", \",#+\", \",,*\" and \",,#+\".

NOTE: this is exact copy of `org-unescape-code-in-region'. This is
to avoid adding dependency to org-mode during component build.
"
  (save-excursion
    (goto-char end)
    (while (re-search-backward "^[ \t]*,*\\(,\\)\\(?:\\*\\|#\\+\\)" beg t)
      (save-excursion (replace-match "" nil nil nil 1))))
  (gix:info "un-escaped code block"))
(cl-defun gix:litconfig/resolve-path (path &optional rel-dir)
  "Give PATH resolve it relative to REL-DIR.

When REL-DIR is not available, return PATH as is without resolving it."

  (setq path (format "%s" path))

  (cond
   (;; path is invalid?
	(string-prefix-p "../.." path)
    (error "resolving PATH by more than a level is not supported"))
   (;; path is relative to REL-DIR:
    (string-prefix-p "./" path)
    (cl-assert
     (stringp rel-dir)
     'SHOW-ARGS
     "path [%s] is relative, but no root directory specified"
     path)
    (setq path (substitute-env-in-file-name path))
    (setq path (expand-file-name path rel-dir)))
   (;; path is +1 level up
    (string-prefix-p "../" path)
    (cl-assert
     (stringp rel-dir) 'SHOW-ARGS
     "path [%s] is relative, but no root directory specified"
     path)
    (setq path (substitute-env-in-file-name path))
    (setq path (expand-file-name path rel-dir)))
   (t;; the default is to treat path as full path
    ;; or try expanding it w.r.t the current working directory??
    ;; TODO: this could be troublesome??? test
    (setq path (substitute-in-file-name path))
    (setq path (expand-file-name path)))))

;;;////////////////////////////////////////////////////////////////////////////
;;; internal org parser
;;;////////////////////////////////////////////////////////////////////////////
(defun gix:litconfig/parse-org (manifest-info org-file lib-map sub-map)
  "Scan ORG-FILE and populate LIB-MAP and SUB-MAP with the scanned code blocks respectively."

  (cl-assert
   (file-exists-p org-file)
   'SHOW-ARGS
   "gix:litconfig/parse-org: arg ORG-FILE does not exist")

  (let* ((org-dir              (file-name-directory org-file))
         (default-lib-name     (gix:litconfig/comp-name-from-org org-file))
         (bin-dir              (plist-get  manifest-info  :bin-dir))
         (etc-dir              (plist-get  manifest-info  :etc-dir))
         (share-dir            (plist-get  manifest-info  :share-dir))
         (elisp-site-dir       (plist-get  manifest-info  :elisp-site-dir))
         (guile-site-dir       (plist-get  manifest-info  :guile-site-dir))
         (src-block-rx         (concat
                                ;; (1) indentation                 (2) lang
                                "^\\([ \t]*\\)#\\+begin_src[ \t]+\\([^ \f\t\n\r\v]+\\)[ \t]*"
		                        ;; (3) switches
                                "\\([^\":\n]*\"[^\"\n*]*\"[^\":\n]*\\|[^\":\n]*\\)"
                                ;; (4) header arguments
                                "\\([^\n]*\\)\n"
                                ;; (5) body
                                "\\([^\000]*?\n\\)??[ \t]*#\\+end_src")))

    (gix:info "scan code blocks: <%s> ..." org-file)

    (with-temp-buffer
      (insert-file-contents org-file)
      (read-only-mode t)
      (goto-char (point-min))
      (while (re-search-forward src-block-rx nil t)
        (let* ((lib          nil)
               (out-file     nil)
               (out-map      nil)
               (body-list    ())
               (lang         (match-string 2))
               (args         (match-string 4))
               (body         (match-string 5))
               (ignore-block (or (gix:litconfig/ignore-current-section-p)
                                 (string-match-p ":tangle\\s-+no[ \t]*\\(\n\\|:\\)" args))))
          ;; is code block ignored?
          ;; *Note*: only code blocks that are not direct decendants of an INGNORED node
          ;; or those not marked with 'tangle: no' attribute are considered.
          (unless ignore-block
            ;; resolve lang
            ;; map: elisp, emacs-lisp   => elisp
            ;; map: shell, sh, ...      => shell
            (setq lang (format "%s" lang))
            (setq lang (cond (;; is lang elisp?
                              (or (string= "elisp"      lang)
                                  (string= "emacs-lisp" lang))
                              "elisp")
                             (;; shell?
                              (or (string= lang "shell")
                                  (string= lang "sh")
                                  (string= lang "bash")
                                  (string= lang "zsh"))
                              "shell")
                             (t;; default
                              lang)))
            ;; resolve code block output library name or file path
            (setq lib  (cond (;; :tangle is specified
                              (string-match ":tangle[ \t]+\\([^ \f\t\n\r\v]+\\)[ \t]*" args)
                              (match-string 1 args))
                             (;; :tangle is not specified but lang = elisp
                              ;; assume it points to the default elisp library name
                              (string= "elisp" lang)
                              default-lib-name)
                             (t ;;unknonwn code block
                              (user-error "code block does not specify language: %s" body-list))))

            ;; prepare library or substitution map entry info
            ;; determin current source block'
            ;; - out-map  : decide between sub-map & lib-map
            ;; - out-file : construct output file name
            (cond

             ;; misc
             (;; is an append block?
              (string-prefix-p "<<append:" lib)
              (setq out-file (substring lib 9 -2))
              ;; substitue environment variables in file-name
              (setq out-file (gix:litconfig/resolve-path out-file org-dir))
              (gix:sub-info "found: <%s> append block =>  [%s]" lang out-file)
              (setq out-file (format "<<append:%s" out-file))
              (setq out-map sub-map))
             (;; is a substitution block?
              (string-prefix-p "<<" lib)
              (setq out-file lib)
              (setq out-map sub-map)
              (gix:sub-info "found: <%s> substitution code block" lang))

             ;; shell
             (;; is main script block?
              (and  (string= lang "shell") (string= lib "main"))
              (setq  out-file  (expand-file-name
                                (concat default-lib-name ".sh")
                                bin-dir))
              (setq out-map   lib-map)
              (gix:sub-info "found: shell main block for: %s" out-file))
             (;; is a bin script block?
              (and (string= lang "shell") (string-prefix-p "bin" lib))
              (setq  out-file  (expand-file-name
                                (concat "./../" lib (if (string-suffix-p ".sh" lib) "" ".sh") )
                                bin-dir))
              (setq  out-map   lib-map)
              (gix:sub-info "found: shell executable %s" out-file))
             (;; is a configure block?
              (and (string= lang "shell") (string= "configure" lib))
              (setq  out-file  (expand-file-name
                                (concat "./../" lib (if (string-suffix-p ".sh" lib) "" ".sh") )
                                bin-dir))
              (setq  out-map   lib-map)
              (gix:sub-info "found: shell executable %s" out-file))
             (;; is a lib etc file?
              (and (string= lang  "shell")
                   (or (string= "host.env" lib)
                       (string-suffix-p "container.env" lib)
                       (string-suffix-p "container.opt" lib)))
              (setq  out-file  (expand-file-name lib  etc-dir))
              (setq  out-map   lib-map)
              (gix:sub-info "found: shell configuration %s" out-file))
             (;; is man block?
              (and (string= lang "shell") (string= lib "man") )
              (setq out-file (expand-file-name
                              (concat default-lib-name ".sh")
                              bin-dir))
              (setq out-map  lib-map)
              (setq body     (with-temp-buffer
                               (insert ": << DOC\n")
                               (insert body "\n")
                               (insert "<< DOC")
                               (buffer-string)))
              (gix:sub-info "found: man block for: %s" out-file))

             ;; elisp
             (;; is elisp library block?
              (string= lang "elisp")
              (if (string-suffix-p ".el" lib)
                  ;; lib has  .el extension
                  (setq out-file (gix:litconfig/resolve-path lib))
                ;; otherwise
                (setq  out-file  (expand-file-name (concat lib ".el")   elisp-site-dir)))
              (setq out-map   lib-map)
              (gix:sub-info "found: elisp library block for: %s" out-file))
             ;; scheme
             (;; is a channel spec?
              (and (string= lang "scheme") (string= lib "channels"))
              ;; resolve library
              (setq out-file  (substitute-in-file-name "${HOME}/.config/guix/channels.scm"))
              (if (file-exists-p out-file)
                  ;; if out-file exisits, treat block as append block
                  (progn (setq out-file (format "<<append:%s" out-file))
                         (setq out-map  sub-map))
                ;; otherwise, treat block as standalon library file
                (setq out-map lib-map))
              (gix:sub-info "found: scheme channel => [%s]" out-file))
             (;; is a scheme manifest?
              (and (string= lang "scheme") (string= lib "manifest"))
              (setq out-file  (expand-file-name (concat lib ".scm")   guile-site-dir))
              (setq out-map   lib-map)
              (gix:sub-info "found: Guix manifest: %s" out-file))
             (;; is scheme library?
              (string= lang "scheme" )
              (if (string-suffix-p ".scm" lib)
                  ;; if the library has .scm extension, then treat it like
                  ;; relative or full path
                  (setq out-file (gix:litconfig/resolve-path lib))
                ;; otherwise, output it to the default cache site
                (setq  out-file  (expand-file-name (concat lib ".scm")   guile-site-dir)))
              (setq out-map  lib-map)
              (gix:sub-info "found: scheme library for: %s" out-file))


             (;; etc block?
              (or
               (s-prefix-p  lib "etc/"))
              (setq out-file  (expand-file-name (substring  lib 4))  etc-dir)
              (setq out-map  lib-map)
              (gix:sub-info "found: configuration file : %s" out-file))

             (;; share block?
              (s-prefix-p  "share/" lib)
              (setq out-file (expand-file-name (substring  lib 6)  share-dir))
              (setq out-map  lib-map)
              (gix:sub-info "found: shared file : %s" out-file))

             ;; other
             (t;;
              ;; default is to tangle source block
              ;; either as relative file to the org doc
              ;; or to treat the file as a full path
              (setq out-file (gix:litconfig/resolve-path lib org-dir))
              (setq out-map  lib-map)
              (gix:sub-info "found: %s source-block for: [%s]" lang out-file)))

            ;; write collected codeblock to output map:
            (cl-assert (not (null out-map)) 'SHOW-ARGS "output map could not be resolved")
            (let ((out-key (cons lang out-file)))
              (setq  body-list (gethash out-key out-map body-list))
              (cl-pushnew body body-list :test-not)
              (puthash out-key body-list out-map))))))
    (gix:info "scan code blocks: <%s> ... [DONE]" org-file)))
(defun gix:litconfig/apply-substitution ( lang lib-file sub-map )
  "Applies a 3-pass substitution using SUB-MAP for the current buffer to be exported.

Note:
- pass 1 only does append
- pass 2 and 3 apply substitution."

  (gix:info "applying 2-pass substitutions for(%s) <%s>:" lang lib-file )
  (cl-loop
   for npass from 1 to 3
   do
   (progn
     (gix:info "substitution pass# %s: ..." npass)
     ;; foreach substitution in sub-map
     ;; 1. pass 1: apply apply substitution by append
     ;; 2. pass 2 and 3: apply substitution by replacement
     (maphash

      ;;map function
	  (lambda (sub-key sub-body-list)
	    (let ((sub-lang     (car sub-key))
		      (sub-tag      (cdr sub-key))
		      (sub-file))
	      (cond (;; is substition with append?
		         (string-prefix-p "<<append:" sub-tag)
                 ;; skip <<append: tag
		         (setq sub-file (substring sub-tag 9))
		         (when (and (= npass 1) (string= sub-file lib-file))
		           (goto-char (point-max))
		           (newline)
		           (apply 'insert (reverse  sub-body-list))
		           (gix:sub-info "target file: %s" sub-file)
		           (gix:sub-info "appended: block %s to: %s" sub-tag lib-file)))
		        (;; is substitution with replace?
		         (string-prefix-p "<<" sub-tag)
		         (when (and (> npass 1) (string= sub-lang lang))
		           (goto-char (point-min))
		           (while (search-forward sub-tag nil t)
			         (replace-match "")
			         (apply 'insert (reverse sub-body-list))
			         (gix:sub-info "substituted: block %s occurance in %s" sub-tag  lib-file))))
		        (;; is substitution is invalid and is skipped?
		         t
		         (gix:sub-info
                  "skipped substition of [ %s ]  for [ %s/(%s) ]"
                  sub-tag lib-file  lang)))))

	  ;;substition map
	  sub-map))))
(defun gix:litconfig/export-libs (lib-map sub-map manifest-info )
  "Export each library in LIB-MAP to source code file
- SUB-MAP: substitution map used to look-up template source blocks
- PROFILE-NAME: Guix profile where exported packages will be installed"

  (cond   (;; library map is not empty?
           (> (hash-table-count lib-map) 0)
           ;; export libraries with their substitution
           (gix:info "exporting libraries ..." )
           (maphash

            ;;export function
            `(lambda (lib-key  body-list)
               (gix:litconfig/export-lib
                lib-key
                (reverse body-list)
                ,sub-map
                ',manifest-info))

            ;;library map
            lib-map)
           t)
          (t ;; libray map is empty?
           (gix:info "Empty lib-map, nothing to export.")
           nil)))
(defun gix:litconfig/export-lib  (lib-key body-list sub-map manifest-info)
  "Export BODY-LIST as a standlon file as specified by LIB-KEY.

LIB-KEY    : is a cons of the form (lang .  lib-info)
             where lib-info can be a file path or library name(for elisp files).
BODY-LIST  : is a list of code blocks for the library specified by lib-key.
SUB-MAP    : is a hash-map of the form [ (<lang> . <sub-tag>) body-list ]
             where
             <lang>    : is the code language associated with the source code block
             <sub-tag> : is substitution string that can be used to lookup string
                         tags to be repaced with body-list of the associated substitution
                         map."
  (let* ((lang          (car lib-key))
         (lib-file      (cdr lib-key))
         (lib-dir       nil)
         (lib-name      nil))
    (cond (;; export a lisp file?
           (string= lang "elisp")
           ;; construct library information
           (setq lib-name  (file-name-sans-extension (file-name-nondirectory lib-file)))
           ;; export library
           (gix:sub-info "exporting elisp code block to: [%s]" lib-file)
           ;;(gix:fs/dir-ensure lib-dir)
           (with-temp-buffer
             (setq buffer-file-coding-system 'utf-8)
             (insert ";;; -*- lexical-binding: t; coding: utf-8; -*-auto generated config package(DO NOT EDIT)\n")
             (insert ";;; Commentary: Auto generated library.\n")
             (insert ";;; Code:\n")
             (apply 'insert body-list)
             (insert "\n\n (provide '" lib-name ")\n")
             (insert ";;; " lib-name ".el ends here.")

             ;; apply substitutions
             (gix:litconfig/apply-substitution lang lib-file sub-map)

             ;; write file to disk
             (gix:litconfig/save-buffer-as lib-file))
           ;; scan source file for Guix packages
           (gix:manifest/scan manifest-info lib-file))
          (;; all non-lisp libraries are exported here
           (and lang (or (symbolp lang) (stringp lang)))
           ;; set library information
           (setq lib-name (file-name-nondirectory lib-file))
           (gix:sub-info "exporting <%s> code block to: [%s]" lang lib-file)
           (with-temp-buffer
             (setq buffer-file-coding-system 'utf-8)
             (goto-char (point-min))
             ;;  content
             (apply 'insert body-list)
             ;; substitution
             (gix:litconfig/apply-substitution lang lib-file sub-map)
             ;;unescape *

             (gix:litconfig/unescape-code-in-region (point-min) (point-max))
             ;; save to file

             (gix:litconfig/save-buffer-as lib-file))))))

;;;////////////////////////////////////////////////////////////////////////////
;;; Main
;;;////////////////////////////////////////////////////////////////////////////
;;;###autoload
(cl-defun gix:litconfig/apply
    (org-file
     &key
     (update-channels nil)
     (cache-dir       gix:fs-user-cache-dir))
  "Build emacs-lisp and guile-lisp library given an ORG-FILE.

- CACHE-DIR: cache dir for storing generated files
"

  (let ((ctime              (current-time))
        (comp-name          (gix:litconfig/comp-name-from-org org-file))
	    (comp-dir           (gix:litconfig/comp-dir-from-org  org-file))
        (manifest-info       nil)
        (elisp-site-dir      nil)
        (guile-site-dir      nil)
        (bin-dir             nil)
        (etc-dir             nil)
        (share-dir           nil)
        (result-ok           t))

    (setq manifest-info  (gix:manifest/init comp-name comp-dir cache-dir))

    (setq bin-dir        (plist-get manifest-info :bin-dir))
    (setq etc-dir        (plist-get manifest-info :etc-dir))
    (setq share-dir      (plist-get manifest-info :share-dir))
    (setq elisp-site-dir (plist-get manifest-info :elisp-site-dir))
    (setq guile-site-dir (plist-get manifest-info :guile-site-dir))

    (gix:info "elisp site: %s"          elisp-site-dir)
    (gix:info "guile site: %s"          guile-site-dir)
    (gix:info "guile site(default): %s" guile-site-dir)

    ;; delete old auto generated  bin files
    (gix:litconfig/cache-delete-files bin-dir)
    (gix:litconfig/cache-delete-files etc-dir)
    (gix:litconfig/cache-delete-files share-dir)
    (gix:litconfig/cache-delete-files guile-site-dir)
    (gix:litconfig/cache-delete-files elisp-site-dir)

    ;; scan org-file for source blocks
    (let ((sub-map       (make-hash-table :test 'equal))
          (lib-map       (make-hash-table :test 'equal))
          (org-dir       (file-name-directory org-file)))

      ;; collect all source and substition blocks
      (gix:litconfig/parse-org
       manifest-info
       org-file
       lib-map
       sub-map)


      (gix:fs/dir-ensure elisp-site-dir)
      (gix:fs/dir-ensure guile-site-dir)

      ;; export each source block to file system.
      (setq result-ok
            (gix:litconfig/export-libs
             lib-map
             sub-map
             manifest-info))


      (unless result-ok (error "failed to export libs"))

      ;; create org-file lock to sync it against the exported  files
      (when result-ok
        (gix:sub-info "building manifest ...")
        (gix:manifest/build manifest-info)
        (gix:sub-info "finished in [%s sec]" (time-to-seconds (time-since  ctime)))))

    ;; return result
    result-ok))

(provide 'gix-litconfig)
;;; gix-litconfig.el ends here
