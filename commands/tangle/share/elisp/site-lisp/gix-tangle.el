;;; gix-tangle.el --- tangle org file -*- lexical-binding: t; coding: utf-8;  -*-

;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Git.
;;; Git is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Git is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Git.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:
;;; Given a literate config file, tangles it to multiple output files.
;;; Code:

(require 'gix-litconfig)
(require 'gix-log)

;; OPTIONS
(setq debugger-stack-frame-as-list t)

;; ARGS
;; arg: org-file
(defconst org-file (car (last command-line-args))
  "The org file to process")

;; DEBUG INFO
(gix:info "gix-tangle: GNU/Emacs:%s"    (version))
(gix:info "gix-tangle: Org-Mode: %s"    (org-version))
(gix:info "gix-tangle: target file: %s" org-file)

(gix:litconfig/apply org-file)
