;;; Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)
;;;
;;; This file is part of Gix.
;;; Gix is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; Gix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with Gix.  If not, see <https://www.gnu.org/licenses/>.

(define-module ( {{MODULE}} )
  ;; standard emacs modules
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix  packages)
  #:use-module (guix  download)
  #:use-module (guix  git-download)
  #:use-module (guix  build-system gnu)
  #:use-module (guix  build-system cmake)
  #:use-module (guix  build-system emacs)
  #:use-module (guix  build-system glib-or-gtk)
  #:use-module (guix  build-system perl)
  #:use-module (guix  build-system trivial)
  #:use-module (gnu   packages)
  #:use-module (guix   utils)
  #:use-module (srfi  srfi-1)
  {{USE-MODULES}})


(define-public {{PACKAGE}}
  (let ((version "{{VERSION}}")
        (commit  "{{COMMIT}}")
        (revision "{{REVISION}}"))
    (package
     (name    "{{PACKAGE}}")
     (version (git-version version revision commit))
     (source   (origin
                (method      git-fetch)
                (uri        (git-reference
                             (url    "{{GIT}}")
                             (commit commit)))
                (file-name  (git-file-name name version))
                (sha256     (base32
                             "{{SHA256}}"))))
     {{INPUTS}}
     {{NATIVE-INPUTS}}
     {{PROPAGATED-INPUTS}}
     (build-system {{BUILD-SYSTEM}})
     {{ARGUMENTS}}
     (synopsis "Auto generated guix packae")
     (description "This packae is auto generated")
     (home-page   "https://www.gitlab.com/aronggile/emacs-duddits")
     (license license:gpl3+)
     )))

;; ;;; enable guix package installation via --from-file option
;; {{PACKAGE}}
