# Copyright (C) 2021 Aaron Gile(aronggile@gmail.com)

# This script is part of Gix.

# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

#==============================================================================
# OPTIONS
#==============================================================================
set   -e
set   -o pipefail
# =============================================================================
# IMPORTS
# =============================================================================
. "${GIX_DIR}/stdlib/share/sh/gix-log.sh"
. "${GIX_DIR}/commands/find/share/sh/gix-comp-find.sh"
# =============================================================================
# FUNCTIONS
# =============================================================================
gix_comp_tangle() {
    # Tangles COMPONENT into one or more output files
    #
    # Where
    # - COMP($1)      : the input org-mode file to tangle
    # - COMP_DIR($2)  : optional component root directory
    #
    # Returns:
    # - zero on success, otherwise none zero.

    local _comp="$1"
    local _comp_dir="$2"
    [ -d "${_comp_dir}" ] || {
	    _comp_dir=$(gix_comp_find "${_comp}")
        [ -d "${_comp_dir}" ] || {
            gix_error "failed to find component: ${_comp_dir}"
            return 1
        }
    }

    local _org_file="${_comp_dir}/${_comp}.org"
    local _comp_lock_dir="${HOME}/.cache/gix/cache/components/${_comp}"
    local _org_lock="${_comp_lock_dir}/${_comp}.org.lock"


    [ -f "${_org_file}"  ] || {
	    gix_info "tangle: component<${_comp}> has no org file: ${_org_file}"
        gix_info "tangle: nothing to tangle"
        return 0
    }
    gix_info "check lock: ${_org_lock}"
    sha256sum -c "${_org_lock}" 1>& /dev/null &&  {
        # org-file has not changed since last time
        return 0
    }

    # TODO: Fix
    # org-file may contain default manifest definition
    # as part of the org config file. When the config
    # file changes, the manifest is updated accordingly.
    # However, users may also edit the manifest directly.
    # That is, outside of the org-file. In this case,the org-file
    # should be tangled to sync with the manifest.
    # But this means edits applied to the manifest will be lost on sync.
    # How to avoid it?
    # prompt user?
    #===========================================================================
    # Run Tangle
    #===========================================================================

    gix_info  "tangle: [${_comp}] ..."

    ## EMACS LOAD PATHS
    local shared_site_dir="${GIX_DIR}/stdlib/share/elisp/site-lisp"
    local local_site_dir="${GIX_DIR}/commands/tangle/share/elisp/site-lisp"

    # run tangle
    local gix_tangle_el="${GIX_DIR}/commands/tangle/share/elisp/site-lisp/gix-tangle.el"
    guix environment                                                                 \
         --ad-hoc coreutils emacs-minimal  emacs-use-package                         \
         -- env                                                                      \
         GIX_USER_CONFIG_DIR="${GIX_USER_CONFIG_DIR}"                                \
         GIX_USER_CACHE_DIR="${GIX_USER_CACHE_DIR}"                                  \
         GIX_PROJ_CONFIG_DIR="${GIX_PROJ_CONFIG_DIR}"                                \
         GIX_PROJ_CACHE_DIR="${GIX_PROJ_CACHE_DIR}"                                  \
         GIX_GUIX_PROFILES_DIR="${GIX_GUIX_PROFILES_DIR}"                            \
         EMACSLOADPATH="${shared_site_dir}:${local_site_dir}:${EMACSLOADPATH}"       \
         emacs -Q --script ${gix_tangle_el} ${_org_file}
    [  $? -eq 0 ] || {
        # tangle failed?
        gix_error "tangle: [${_comp}] ... [FAILED]"
	    return 1
    }

    # lock org-file
    mkdir -p "${_comp_lock_dir}" || {
        gix_error "failed to mkdir:${_comp_lock_dir}"
        return 1
    }
    sha256sum "${_org_file}"  > "${_org_lock}" || {
        gix_error "failed to create lock file: ${_org_lock}"
        return 1
    }

    gix_info "tangle: [${_comp}] ... [OK]"

    return 0
}
