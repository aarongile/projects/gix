#! /usr/bin/env sh

# Copyright (C) 2021 <Aaron Gile(aronggile@gmail.com)>

# This script is part of Gix.

# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

# =============================================================================
# IMPORTS
# =============================================================================
. ${GIX_DIR}/stdlib/share/sh/gix-log.sh
. ${GIX_DIR}/commands/tangle/share/sh/gix-comp-tangle.sh
#==============================================================================
# OPTIONS
#===============================================================================
set   -e
set   -o pipefail
# =============================================================================
# MAIN
# =============================================================================

readonly gix_comp="$1";
readonly gix_comp_dir="$2";
gix_comp_tangle "${gix_comp}" "${gix_comp_dir}" || exit 1
exit 0
