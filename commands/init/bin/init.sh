#!/usr/bin/env bash

# Copyright (C) 2021 <Aaron Gile(aronggile@gmail.com)>

# This script is part of Gix.

# Gix is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Gix is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Gix.  If not, see <https://www.gnu.org/licenses/>.

# Commentary:
#
# Initialize gix in the current directory.
#
#   $1: CURRENT_WORKING_DIR (default: GIX_CWD)
# OUTPUTS:
#   Initializes gix in the current working directory
#   directory if it is not already initialized.
# RETURN:bizunesh gizaw
#   0 if it succeeds, non-zero on error.
# USAGE:
# [ env GIX_VERBOSE=true ] gix init [working-directory]


source "${GIX_DIR}/stdlib/share/sh/gix-log.sh"

# ===========================================================================================
# main
# ===========================================================================================

gix_cwd="${1:-$GIX_CWD}"

# is valid directory
[[ -d "${gix_cwd}" ]] || {
    gix_error "directory not found: ${gix_cwd}"
    exit 1
}

# directory already initialized?"
[[ -d "${gix_cwd}/.config/gix" ]] && {
    gix_info "nothing to do! directory already initialized"
    exit 0
}


gix_config_dir="${gix_cwd}/.config/gix"
mkdir -p "${gix_config_dir}" || {
    gix_error "failed to create gix config directory: ${gix_config_dir}"
    exit 1
}

gix_info "gix initialized in: ${gix_cwd}"
exit 0

: <<-'DOC'

=pod

=head1 init

 gix init : initializes the current directory as a gix project

=head1 SYNOPSIS

 gix init [ --verbose|-v ]

=head1 DESCRIPTION

gix init is a utility script that initializes the current
direcory as a gix project. A gix project on top of a
project org(literate) file contains a .config and
.cache directories for storing project configuration
and cache files respectively.

=cut

EOF
