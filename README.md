# Gix

Gix is a literate programming environment that extends Emacs org-mode
with specialized source blocks that enable GNU/Guix based package
management per document. Gix provides a CLI client `gix`, which can
run org documents directly from the command-line or in a
containerized environment.

## Requirements

 - <a href="https://guix.gnu.org/manual/en/html_node/Installation.html">Gnu/Guix Package Manager</a>

## Installing

To install Gix first clone this repository to some directory of
choice,`~/opt/gix` for example.

``` sh

mkdir -p ~/opt
cd ~/opt
git clone https://gitlab.com/aarongile/projects/gix

```

Next integrate Gix with your shell environment by adding
the snippet below-- Bash `~/.bashrc`, Zsh `~/.zshenv`.
``` sh
# point this variable to the your Gix install directory
export GIX_DIR=${HOME}/opt/gix

# point this variable to the root of your Gix document directory.
# Org documents in this directory can be accessed from
# anywehre when using gix command.
export GIX_COMPDIR=${HOME}/Documents/gix-components

# source the gix environment setup, which defines
# necessary environment variables and adds the gix command
# to PATH
. ${GIX_DIR}/.gixrc

```

Finally start a new shell and run the Gix configuration
command to finalize the install:
``` sh

gix configure

```

That's it, Gix is installed and ready to go!

## Example

Create a `hello.org` file, and edit its content,
``` org
#+title: hello world

This is a hello world literate program using Gix.
It displays a greeting message using the Python.

#+begin_src shell :tangle main
 #! /usr/bin/env python3

 print("Hello World!")

#+end_src

To run this document =python= has to be available locally.
The dependency can be specified using a scheme manifest
source block
#+begin_src scheme  :tangle manifest

(specifications->manifest '("python"))

#+end_src

```
This document contains a `python` source block, that prints
`Hello World!`. It also specifies `python` as a dependency
that needs to be installed. This is regardless of wether the
host system has `python` or not. It is installed in an isolated
Guix profile.

To run the document execute,
```sh
gixd hello
```

## Documentation

 For more information please *see* https://simfish.dev/projects/gix

## Status

 This project was initally written for my presonal use only.
 Almost the entirety of this project has only been tested on a single
 machine running Fedora. Thing will definetly break.

## Version History

* 0.0.1

## License

This project is licensed under the GPL-3.0-only or GPL-3.0-or-later - see the COPYING file for details
